package com.ktamr.management.meter;

import com.ktamr.common.core.domain.BaseController;
import com.ktamr.common.utils.DateUtils;
import com.ktamr.domain.*;
import com.ktamr.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/meter")
public class MeterController extends BaseController {

    @Resource
    private HaMeterService haMeterService;

    @Resource
    private HavMeterinfoService havMeterinfoService;

    @Resource
    private HaCmdService haCmdService;

    @Resource
    private HaRecordsService haRecordsService;

    @Resource
    private HaDayFreezeService haDayFreezeService;

    @Resource
    private HaMonFreezeService haMonFreezeService;

    @Resource
    private HaReplaceRecordsService haReplaceRecordsService;

    @RequestMapping("/meter_replace")
    public String meter_replace(HaMeter haMeter, Integer meterid, Model model, HttpSession session){
        String operatorCode = (String)session.getAttribute("operatorCode");
        haMeter.setMeterId(meterid);
        HaMeter meter = haMeterService.delByIdHaMeter(haMeter);
        String thNumber = String.valueOf(meter.getThNumber());
        HavMeterinfo userAddr = havMeterinfoService.userAddr(meterid);
        model.addAttribute("meter",meter);
        model.addAttribute("meterid",meterid);
        model.addAttribute("userAddr",userAddr);
        model.addAttribute("operatorCode",operatorCode);
        model.addAttribute("nowDate", DateUtils.getNowDate());
        model.addAttribute("thNumber",thNumber);
        return "area/meter_replace";
    }

    @RequestMapping("/queryHaMeter")
    @ResponseBody
    public Object queryHaMeter(Integer centorId,Integer collectorId){
        startPage();
        List<HaMeter> haMeters = haMeterService.queryHaMeter(centorId,collectorId);
        Map<String, Object> map = getDataTable(haMeters);
        return map;
    }

    @RequestMapping("/meter_replace_do")
    @ResponseBody
    public Object meterReplaceDo(HaMeter haMeter, HaCmd haCmd, HaRecords haRecords,Integer meterId, HaDayfreeze haDayfreeze,HaMonfreeze haMonfreeze,HaReplacerecords haReplacerecords){
        haMeter.setMeterId(meterId);
        HaMeter meter = haMeterService.delByIdHaMeter(haMeter);
        if(meter!=null){

            Integer addHaCmd = haCmdService.addHaCmd(String.valueOf(haReplacerecords.getOldMeterNumber()),String.valueOf(haReplacerecords.getNewMeterNumber()),String.valueOf(haReplacerecords.getOldMeterNumber()),String.valueOf(haReplacerecords.getNewMeterNumber()));
            Integer addHaRecords = haRecordsService.addHaRecords(String.valueOf(haReplacerecords.getOriRead()),haReplacerecords.getOriRead(),haMeter.getMeterId());

            HaDayfreeze fdayDataCount = haDayFreezeService.fdayDataCount(haMeter.getMeterId());
            if(fdayDataCount!=null){
                Integer fdayDataCount2 = haDayFreezeService.fdayDataCount2((int)haReplacerecords.getOriRead(),(int)haReplacerecords.getOriRead(),haMeter.getMeterId());
            }else {
                Integer fdayDataCount3 = haDayFreezeService.fdayDataCount3((int)haReplacerecords.getOriRead(),(int)haReplacerecords.getOriRead(),haMeter.getMeterId());
            }

            HaMonfreeze fmonDataCount = haMonFreezeService.fmonDataCount(haMeter.getMeterId());
            if(fmonDataCount!=null){
                Integer fmonDataCount2 = haMonFreezeService.fmonDataCount2((int)haReplacerecords.getOriRead(),(int)haReplacerecords.getOriRead(),haMeter.getMeterId());
            }else {
                Integer fmonDataCount3 = haMonFreezeService.fmonDataCount3((int)haReplacerecords.getOriRead(),(int)haReplacerecords.getOriRead(),haMeter.getMeterId());
            }
            HaMeter haMeter1 = new HaMeter();
            haMeter1.setAddr(String.valueOf(haReplacerecords.getNewMeterNumber()));
            haMeter1.setMeterNumber((int)haReplacerecords.getNewMeterNumber());
            haMeter1.setSnumber(haReplacerecords.getOriRead());
            haMeter1.setGnumber(haReplacerecords.getFinalRead());
            haMeter1.setLfNumber(haReplacerecords.getFinalRead());
            haMeter1.setLfTime(haReplacerecords.getReplaceDate());
            haMeter1.setThNumber(haReplacerecords.getFinalRead());
            haMeter1.setThTime(haReplacerecords.getReplaceDate());
            haMeter1.setFdayn(haReplacerecords.getFinalRead());
            haMeter1.setFmonn(haReplacerecords.getFinalRead());
            haMeter1.setMeterId(haMeter.getMeterId());
            Integer replaceMeter = haMeterService.replaceMeter(haMeter1);
            Integer replaceMeter2 = haReplaceRecordsService.replaceMeter2(haReplacerecords);
            Integer replaceMeter3 = haMeterService.replaceMeter3(haMeter);
            return "true";
        }else {
            return "原表资料不存在！";
        }
    }

    @RequestMapping("/addingCellValidation")
    @ResponseBody
    public String addingCellValidation(HaMeter haMeter){
        Integer addingCellValidation = haMeterService.addingCellValidation(haMeter);
        if(addingCellValidation==1){
            return "True";
        }
        return "false";
    }
}
