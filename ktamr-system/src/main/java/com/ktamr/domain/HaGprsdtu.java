package com.ktamr.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.ktamr.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class HaGprsdtu extends BaseEntity {

    private String uId;
    private String telNubmer;
    private String addr;
    private Integer centors;
    private String state;
    private String dscrp;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date oplastTime;
    private Integer idle;

    private HaCentor haCentor;
    private HaArea haArea;
    private String keyWord;
    //存查询时用到的对象
    private String nodeType;
    private String nodeIds;
    private String xsjcqsm;

    public String getXsjcqsm() {
        return xsjcqsm;
    }

    public void setXsjcqsm(String xsjcqsm) {
        this.xsjcqsm = xsjcqsm;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getTelNubmer() {
        return telNubmer;
    }

    public void setTelNubmer(String telNubmer) {
        this.telNubmer = telNubmer;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Integer getCentors() {
        return centors;
    }

    public void setCentors(Integer centors) {
        this.centors = centors;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDscrp() {
        return dscrp;
    }

    public void setDscrp(String dscrp) {
        this.dscrp = dscrp;
    }

    public Date getOplastTime() {
        return oplastTime;
    }

    public void setOplastTime(Date oplastTime) {
        this.oplastTime = oplastTime;
    }

    public Integer getIdle() {
        return idle;
    }

    public void setIdle(Integer idle) {
        this.idle = idle;
    }

    public HaCentor getHaCentor() {
        return haCentor;
    }

    public void setHaCentor(HaCentor haCentor) {
        this.haCentor = haCentor;
    }

    public HaArea getHaArea() {
        return haArea;
    }

    public void setHaArea(HaArea haArea) {
        this.haArea = haArea;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getNodeIds() {
        return nodeIds;
    }

    public void setNodeIds(String nodeIds) {
        this.nodeIds = nodeIds;
    }
}
