package com.ktamr.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.ktamr.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class HaCentor extends BaseEntity {

    private Integer id;
    private long centorId;
    private String ver;
    private String rTime;
    @JsonFormat(pattern = "yyyy-MM-dd") //出参
    @DateTimeFormat(pattern = "yyyy-MM-dd") //入参
    private Date setupTime;
    private String addr;
    private String description;
    private String telNumber;
    @JsonFormat(pattern = "yyyy-MM-dd") //出参
    @DateTimeFormat(pattern = "yyyy-MM-dd") //入参
    private Date endTime;
    private String useTypeCode;
    private String state;
    private String centorNo;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date lastfreezeTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastfreezeDay;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastfreezeMon;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifyTime;
    private Integer meters;
    private Integer areaId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastStateChangeTime;
    private String remark;
    private Integer rssi;
    private Integer vol;
    private String imsi;
    private String imei;

    private Integer haCentorCount;

    private HaArea haArea;
    private HaCollector haCollector;
    private HaMeter haMeter;
    private HavMeterinfo havMeterinfo;

    private Integer zbs;
    private Integer jdbs;
    private Integer wfhbs;

    private String centorDs;
    private String centorAddr;
    private String collectorAddr;
    private String xhqd;
    private String dcdy;
    private String centorAreaNo;
    private String roomMeterId;

    //存查询时用到的对象
    private String nodeType;
    private String nodeIds;

    //表类型
    private String deviceType;

    //表名
    private String cmdName;

    //登录绘画的账号
    private String operator_codeSession;

    public HavMeterinfo getHavMeterinfo() {
        return havMeterinfo;
    }

    public void setHavMeterinfo(HavMeterinfo havMeterinfo) {
        this.havMeterinfo = havMeterinfo;
    }

    public Integer getZbs() {
        return zbs;
    }

    public void setZbs(Integer zbs) {
        this.zbs = zbs;
    }

    public Integer getJdbs() {
        return jdbs;
    }

    public void setJdbs(Integer jdbs) {
        this.jdbs = jdbs;
    }

    public Integer getWfhbs() {
        return wfhbs;
    }

    public void setWfhbs(Integer wfhbs) {
        this.wfhbs = wfhbs;
    }

    public HaMeter getHaMeter() {
        return haMeter;
    }

    public void setHaMeter(HaMeter haMeter) {
        this.haMeter = haMeter;
    }

    public long getCentorId() {
        return centorId;
    }

    public void setCentorId(long centorId) {
        this.centorId = centorId;
    }

    public HaCollector getHaCollector() {
        return haCollector;
    }

    public void setHaCollector(HaCollector haCollector) {
        this.haCollector = haCollector;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getrTime() {
        return rTime;
    }

    public void setrTime(String rTime) {
        this.rTime = rTime;
    }

    public Date getSetupTime() {
        return setupTime;
    }

    public void setSetupTime(Date setupTime) {
        this.setupTime = setupTime;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getUseTypeCode() {
        return useTypeCode;
    }

    public void setUseTypeCode(String useTypeCode) {
        this.useTypeCode = useTypeCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCentorNo() {
        return centorNo;
    }

    public void setCentorNo(String centorNo) {
        this.centorNo = centorNo;
    }

    public Date getLastfreezeTime() {
        return lastfreezeTime;
    }

    public void setLastfreezeTime(Date lastfreezeTime) {
        this.lastfreezeTime = lastfreezeTime;
    }

    public Date getLastfreezeDay() {
        return lastfreezeDay;
    }

    public void setLastfreezeDay(Date lastfreezeDay) {
        this.lastfreezeDay = lastfreezeDay;
    }

    public Date getLastfreezeMon() {
        return lastfreezeMon;
    }

    public void setLastfreezeMon(Date lastfreezeMon) {
        this.lastfreezeMon = lastfreezeMon;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public Date getModifyTime() {
        return modifyTime;
    }

    @Override
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getMeters() {
        return meters;
    }

    public void setMeters(Integer meters) {
        this.meters = meters;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Date getLastStateChangeTime() {
        return lastStateChangeTime;
    }

    public void setLastStateChangeTime(Date lastStateChangeTime) {
        this.lastStateChangeTime = lastStateChangeTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getRssi() {
        return rssi;
    }

    public void setRssi(Integer rssi) {
        this.rssi = rssi;
    }

    public Integer getVol() {
        return vol;
    }

    public void setVol(Integer vol) {
        this.vol = vol;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public HaArea getHaArea() {
        return haArea;
    }

    public void setHaArea(HaArea haArea) {
        this.haArea = haArea;
    }

    public Integer getHaCentorCount() {
        return haCentorCount;
    }

    public void setHaCentorCount(Integer haCentorCount) {
        this.haCentorCount = haCentorCount;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getNodeIds() {
        return nodeIds;
    }

    public void setNodeIds(String nodeIds) {
        this.nodeIds = nodeIds;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getCmdName() {
        return cmdName;
    }

    public void setCmdName(String cmdName) {
        this.cmdName = cmdName;
    }

    public String getCentorDs() {
        return centorDs;
    }

    public void setCentorDs(String centorDs) {
        this.centorDs = centorDs;
    }

    public String getCentorAddr() {
        return centorAddr;
    }

    public void setCentorAddr(String centorAddr) {
        this.centorAddr = centorAddr;
    }

    public String getCollectorAddr() {
        return collectorAddr;
    }

    public void setCollectorAddr(String collectorAddr) {
        this.collectorAddr = collectorAddr;
    }

    public String getXhqd() {
        return xhqd;
    }

    public void setXhqd(String xhqd) {
        this.xhqd = xhqd;
    }

    public String getDcdy() {
        return dcdy;
    }

    public void setDcdy(String dcdy) {
        this.dcdy = dcdy;
    }

    public String getCentorAreaNo() {
        return centorAreaNo;
    }

    public void setCentorAreaNo(String centorAreaNo) {
        this.centorAreaNo = centorAreaNo;
    }

    public String getOperator_codeSession() {
        return operator_codeSession;
    }

    public void setOperator_codeSession(String operator_codeSession) {
        this.operator_codeSession = operator_codeSession;
    }
    public String getRoomMeterId() {
        return roomMeterId;
    }

    public void setRoomMeterId(String roomMeterId) {
        this.roomMeterId = roomMeterId;
    }
}
