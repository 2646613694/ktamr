package com.ktamr.service.impl;

import com.ktamr.domain.HaArea;
import com.ktamr.mapper.HaAuditMapper;
import com.ktamr.mapper.InterfaceDataMapper;
import com.ktamr.service.HaAuditService;
import com.ktamr.service.InterfaceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class InterfaceDataServiceImpl implements InterfaceDataService {

    @Autowired
    private InterfaceDataMapper interfaceDataMapper;

    @Override
    public List<Map<String, Object>> selectV() {
        return interfaceDataMapper.selectV();
    }

}
