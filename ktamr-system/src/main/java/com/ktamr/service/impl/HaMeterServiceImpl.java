package com.ktamr.service.impl;

import com.ktamr.domain.HaCentor;
import com.ktamr.domain.HaCollector;
import com.ktamr.domain.HaMeter;
import com.ktamr.mapper.HaMeterMapper;
import com.ktamr.service.HaMeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 表具资料表实现类
 */
@Service
public class HaMeterServiceImpl implements HaMeterService {
    @Resource
    private HaMeterMapper haMeterMapper;

    @Override
    public Integer updateMeter(HaMeter haMeter) {
        return haMeterMapper.updateMeter(haMeter);
    }

    @Override
    public Integer updateMeterTwo(String importTime) {
        return haMeterMapper.updateMeterTwo(importTime);
    }

    @Override
    public Map<String, Object> selectMeterById(Integer meterid) {
        return haMeterMapper.selectMeterById(meterid);
    }

    /**
     * 查询表具资料表与房间表信息
     * @param meterId 根据id查询
     * @return 返回对象
     */
    @Override
    public HaMeter selectMeterAndBuildingById(Integer meterId) {
        return haMeterMapper.selectMeterAndBuildingById(meterId);
    }

    @Override
    public HaMeter selectMeterAndBuildingByKeyWordTwo(String keyWordTwo) {
        return haMeterMapper.selectMeterAndBuildingByKeyWordTwo(keyWordTwo);
    }

    /**
     * 查询HaMeter表信息
     * @param params 对象参数
     * @return 返回泛型集合对象
     */
    @Override
    public List<HaMeter> selectDosageRecently(HaMeter params) {
        return haMeterMapper.selectDosageRecently(params);
    }

    /**
     * 查询不良表信息
     * @param params 对象参数
     * @return 返回泛型集合
     */
    @Override
    public List<HaMeter> selectNotok(HaMeter params) {
        String[] str;
        if(params.getParams().get("dataType") != null){
            str = params.getParams().get("dataType").toString().split(",");
            for(int i = 0;i<str.length;i++){
                switch (str[i]){
                    case "LightChannelInterference":
                        str[i] = "光通道干扰";
                        break;
                    case "highlightInterference":
                        str[i] = "强光干扰";
                        break;
                    case "bubbleInterference":
                        str[i] = "气泡干扰";
                        break;
                    case "communicationBreadkdown":
                        str[i] = "通讯故障";
                        break;
                    case "meterBreakdown":
                        str[i] = "表具故障";
                        break;
                    case "valveBreakdown":
                        str[i] = "阀门故障";
                        break;
                    case "abnormal":
                        str[i] = "异常";
                        break;
                }
            }
            params.getParams().put("dataTypes",str);
        }
        return haMeterMapper.selectNotok(params);
    }

    @Override
    public List<HaMeter> selectRecordByHand(HaMeter haMeter) {
        return haMeterMapper.selectRecordByHand(haMeter);
    }

    /**
     * 根据centorId查询meter表信息
     * @param centorId 参数centorId
     * @return 返回泛型集合对象
     */
    @Override
    public List<HaMeter> selectMeterByCentorId(Integer centorId) {
        return haMeterMapper.selectMeterByCentorId(centorId);
    }

    /**
     * 根据CollectorId查询meter表信息
     * @param collectorId 参数CollectorId
     * @return 返回泛型集合对象
     */
    @Override
    public List<HaMeter> selectMeterByCollectorId(Integer collectorId) {
        return haMeterMapper.selectMeterByCollectorId(collectorId);
    }

    public List<HaMeter> HaMeterList(HaMeter haMeter) {
        return haMeterMapper.HaMeterList(haMeter);
    }

    @Override
    public List<HaMeter> queryHaMeter(Integer centorId, Integer collectorId) {
        return haMeterMapper.queryHaMeter(centorId,collectorId);
    }


    @Override
    public Integer HaMeterCount(HaMeter haMeter) {
        return haMeterMapper.HaMeterCount(haMeter);
    }

    public Integer addHaMeter(HaMeter haMeter) {
        return haMeterMapper.addHaMeter(haMeter);
    }

    public Integer updateHaMeter(HaMeter haMeter) {
        return haMeterMapper.updateHaMeter(haMeter);
    }

    @Override
    public Integer updateHaMeterCollector(HaMeter haMeter, HaCentor haCentor, HaCollector haCollector) {
        return haMeterMapper.updateHaMeterCollector(haMeter,haCentor,haCollector);
    }

    @Override
    public Integer HaMeterCollectorCount(Integer centorId, Integer collectorId, Integer meterIds) {
        return haMeterMapper.HaMeterCollectorCount(centorId,collectorId,meterIds);
    }

    @Override
    public Integer updateHaMeterReadLine(HaMeter haMeter, HaCentor haCentor, Integer readLineId) {
        return haMeterMapper.updateHaMeterReadLine(haMeter,haCentor,readLineId);
    }

    @Override
    public Integer HaMeterReadLineCount(Integer readLineId, Integer meterIds) {
        return haMeterMapper.HaMeterReadLineCount(readLineId,meterIds);
    }

    @Override
    public Integer updateHaMeterNull(HaMeter haMeter) {
        return haMeterMapper.updateHaMeterNull(haMeter);
    }

    @Override
    public Integer HaMeterNullCount(Integer meterIds) {
        return haMeterMapper.HaMeterNullCount(meterIds);
    }

    @Override
    public Integer isPriceUsed(Integer id) {
        return haMeterMapper.isPriceUsed(id);
    }

    public Integer deleteHaMeter(List haMeter) {
        return haMeterMapper.deleteHaMeter(haMeter);
    }

    @Override
    public HaMeter delByIdHaMeter(HaMeter haMeter) {
        return haMeterMapper.delByIdHaMeter(haMeter);
    }

    @Override
    public List<HaMeter> deleteByIdHaMeterC(List haMeter) {
        return haMeterMapper.deleteByIdHaMeterC(haMeter);
    }

    @Override
    public Integer meterCountNum(Integer roomId) {
        return haMeterMapper.meterCountNum(roomId);
    }

    @Override
    public HaMeter orIgNumber(Integer meterId) {
        if(haMeterMapper.orIgNumber(meterId)==null){
            return new HaMeter();
        }
        return haMeterMapper.orIgNumber(meterId);
    }

    @Override
    public HaMeter orImeterNumber(Integer meterId) {
        return haMeterMapper.orImeterNumber(meterId);
    }

    @Override
    public HaMeter mMeterSequence(Integer meterId) {
        return haMeterMapper.mMeterSequence(meterId);
    }

    @Override
    public HaMeter mMeterSequence2(Integer centorId, Integer centorId2) {
        return haMeterMapper.mMeterSequence2(centorId,centorId2);
    }

    /**
     * 主页统计表==》表列表
     * @param haMeter
     * @return 全部数据List集合
     */
    @Override
    public List<HaMeter> zhuYegetStateMeter(HaMeter haMeter) {
        List<HaMeter> haMeterList = haMeterMapper.zhuYegetStateMeter(haMeter);
        if(haMeterList!=null){
            return haMeterList;
        }
        return null;
    }



    @Override
    public Integer replaceMeter(HaMeter haMeter) {
        return haMeterMapper.replaceMeter(haMeter);
    }

    @Override
    public Integer replaceMeter3(HaMeter haMeter) {
        return haMeterMapper.replaceMeter3(haMeter);
    }

    @Override
    public List<HaMeter> getRowIdMeter(HaMeter haMeter) {
        return haMeterMapper.getRowIdMeter(haMeter);
    }

    @Override
    public Integer noCheck(Integer meterId) {
        return haMeterMapper.noCheck(meterId);
    }

    @Override
    public Integer checkButNoSettlement(String startTime,double gnumber,double lfNumber,Integer meterId) {
        return haMeterMapper.checkButNoSettlement(startTime,gnumber,lfNumber,meterId);
    }

    @Override
    public Integer updateNullRoomId(Integer meterId) {
        return haMeterMapper.updateNullRoomId(meterId);
    }

    @Override
    public Integer updateNullRoomId2(Integer roomId, Integer areaId, Integer meterId) {
        return haMeterMapper.updateNullRoomId2(roomId,areaId,meterId);
    }

    @Override
    public Integer deleteAreaMeterFreeze1(List areaId) {
        return haMeterMapper.deleteAreaMeterFreeze1(areaId);
    }

    @Override
    public Integer deleteAreaMeterFreeze2(List areaId) {
        return haMeterMapper.deleteAreaMeterFreeze2(areaId);
    }

    @Override
    public Integer deleteAreaMeterFreeze3(List areaId) {
        return haMeterMapper.deleteAreaMeterFreeze3(areaId);
    }

    @Override
    public Integer deleteAreaMeter(List areaId) {
        return haMeterMapper.deleteAreaMeter(areaId);
    }

    @Override
    public Integer addingCellValidation(HaMeter haMeter) {
        return haMeterMapper.addingCellValidation(haMeter);
    }
}
