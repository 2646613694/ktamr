package com.ktamr.service.impl;

import com.ktamr.domain.HaMeter;
import com.ktamr.domain.HaMonfreeze;
import com.ktamr.mapper.HaMonFreezeMapper;
import com.ktamr.service.HaMonFreezeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class HaMonFreezeServiceImpl implements HaMonFreezeService {

    @Resource
    private HaMonFreezeMapper haMonFreezeMapper;

    @Override
    public Integer insertMonFreeze(HaMeter haMeter) {
        return haMonFreezeMapper.insertMonFreeze(haMeter);
    }

    @Override
    public Integer insertMonFreezeTwo(String importTime) {
        return haMonFreezeMapper.insertMonFreezeTwo(importTime);
    }

    @Override
    public Integer updateMonFreeze(HaMeter haMeter) {
        return haMonFreezeMapper.updateMonFreeze(haMeter);
    }

    @Override
    public Integer updateMonFreezeTwo(String importTime) {
        return haMonFreezeMapper.updateMonFreezeTwo(importTime);
    }

    @Override
    public Integer delHaMonFreeze(List meterId) {
        return haMonFreezeMapper.delHaMonFreeze(meterId);
    }

    @Override
    public Integer selectMonFreezeMeterIdCount(HaMeter haMeter) {
        return haMonFreezeMapper.selectMonFreezeMeterIdCount(haMeter);
    }

    @Override
    public List<HaMonfreeze> selectAllMonfreeze(Map<String, Object> parms) {
        return haMonFreezeMapper.selectAllMonfreeze(parms);
    }

    @Override
    public HaMonfreeze fmonDataCount(Integer meterId) {
        return haMonFreezeMapper.fmonDataCount(meterId);
    }

    @Override
    public Integer fmonDataCount2(Integer tread,Integer lread,Integer meterId) {
        return haMonFreezeMapper.fmonDataCount2(tread,lread,meterId);
    }

    @Override
    public Integer fmonDataCount3(Integer tread,Integer lread,Integer meterId) {
        return haMonFreezeMapper.fmonDataCount3(tread,lread,meterId);
    }
}
