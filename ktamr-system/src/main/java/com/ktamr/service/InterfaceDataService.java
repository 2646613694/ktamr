package com.ktamr.service;

import com.ktamr.domain.HaArea;

import java.util.List;
import java.util.Map;


public interface InterfaceDataService {

    /**
     * 根据大区编号V查询数据
     * @return
     */
    public List<Map<String,Object>> selectV();
}
