package com.ktamr.mapper;

import com.ktamr.domain.HaConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HaConfigMapper {

    Map<String,Object> selectConfigByV(@Param("v") String v);

    List<HaConfig> queryHaConfig(@Param("haConfig") HaConfig haConfig);

    Integer haConfigCount(HaConfig haConfig);

    Integer addHaConfig(HaConfig haConfig);

    Integer deleteHaConfig(HaConfig haConfig);

    Integer updateHaConfig(HaConfig haConfig);
}
