package com.ktamr.mapper;

import com.ktamr.domain.HaMeter;
import com.ktamr.domain.HaMonfreeze;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 *月冻结抄收记录表Mapper
 */
public interface HaMonFreezeMapper {

    public Integer insertMonFreeze(HaMeter haMeter);

    public Integer insertMonFreezeTwo(String importTime);

    public Integer updateMonFreeze(HaMeter haMeter);

    public Integer updateMonFreezeTwo(String importTime);

    Integer delHaMonFreeze(List meterId);

    public Integer selectMonFreezeMeterIdCount(HaMeter haMeter);

    /**
     * 查询月冻结抄收记录表信息
     * @param parms Map参数
     * @return 返回泛型集合
     */
    public List<HaMonfreeze> selectAllMonfreeze(Map<String,Object> parms);

    //加入月冻结数据
    HaMonfreeze fmonDataCount(@Param("meterId") Integer meterId);

    Integer fmonDataCount2(@Param("tread") Integer tread,@Param("lread") Integer lread,@Param("meterId") Integer meterId);

    Integer fmonDataCount3(@Param("tread") Integer tread,@Param("lread") Integer lread,@Param("meterId") Integer meterId);

}
