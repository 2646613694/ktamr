package com.ktamr.mapper;

import java.util.List;
import java.util.Map;

/**
 * 接口数据上传查询
 */
public interface InterfaceDataMapper {

    /**
     * 根据大区编号V查询数据
     * @return
     */
    public List<Map<String,Object>> selectV();
}
