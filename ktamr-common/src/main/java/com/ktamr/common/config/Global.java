package com.ktamr.common.config;

import com.ktamr.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局配置类
 * 
 * @author ktamr
 */
public class Global
{
    private static final Logger log = LoggerFactory.getLogger(Global.class);

    public static JdbcTemplate jdbcTemplate;

    /**
     * 当前对象实例
     */
    private static Global global;

    /**
     * 保存全局属性值
     */
    private static Map<String, String> map = new HashMap<String, String>();

    private Global()
    {
    }

    /**
     * 静态工厂方法
     */
    public static synchronized Global getInstance()
    {
        if (global == null)
        {
            global = new Global();
        }
        return global;
    }

    /**
     * 获取配置
     */
    public static String getConfig(String key)
    {
        String value = map.get(key);
        if (value == null)
        {
            Map<?, ?> yamlMap = null;
            try
            {
                String sql = " select v from ha_config where s='"+key+"'";
                jdbcTemplate.execute(sql);
                value = jdbcTemplate.queryForObject(sql,java.lang.String.class);
                map.put(key, value != null ? value : StringUtils.EMPTY);
            }
            catch (Exception e)
            {
                log.error("获取全局配置异常 {}", key);
            }
        }
        return value;
    }

    /**
     * 获取log文件路径
     */
    public static  String getLogFilePath(){
        return getConfig("log文件路径");
    }

    /**
     * 获取文件上传路径
     */
    public static String getProfile()
    {
        return getConfig("文件上传路径");
    }

    /**
     * 获取导出路径
     * @return
     */
    public static String getExportPath()
    {
        return getConfig("导出路径");
    }

    /**
     * 获取导出DBF路径
     * @return
     */
    public static String getExportPathDBF()
    {
        return getConfig("导出DBF路径");
    }

    /**
     * 获取导出Txt路径
     * @return
     */
    public static String getExportPathTxt()
    {
        return getConfig("导出Txt路径");
    }

    /**
     * 获取导出Excel路径
     * @return
     */
    public static String getExportPathExcel()
    {
        return getConfig("导出Excel路径");
    }

}
