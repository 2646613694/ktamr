package com.ktamr.common.utils.export;

import com.ktamr.common.config.Global;
import com.ktamr.common.utils.DateUtils;

import java.io.File;
import java.util.UUID;

/**
 * 导出 String类
 *
 * @author ktamr
 */
public class ExportStr {

    public static String encodingFileExcelname()

    {
        return "kt-table"+ DateUtils.dateTimeNow()+".xlsx";
    }

    public static String encodingFileTxtname()
    {
        return "kt-table"+ DateUtils.dateTimeNow()+".txt";
    }

    public static String encodingFileDbfname()
    {
        return "kt-table"+ DateUtils.dateTimeNow()+".dbf";
    }

    /**
     * 获取下载Excel路径
     *
     * @param filename 文件名称
     */
    public static String getAbsoluteFileExcel(String filename)
    {
        String downloadPath = Global.getExportPathExcel() + filename;
        File desc = new File(downloadPath);
        if (!desc.getParentFile().exists())
        {
            desc.getParentFile().mkdirs();
        }
        return downloadPath;
    }

    /**
     * 获取下载Txt路径
     *
     * @param filename 文件名称
     */
    public static String getAbsoluteFileTxt(String filename)
    {
        String downloadPath = Global.getExportPathTxt() + filename;
        File desc = new File(downloadPath);
        if (!desc.getParentFile().exists())
        {
            desc.getParentFile().mkdirs();
        }
        return downloadPath;
    }

    /**
     * 获取下载DBF路径
     *
     * @param filename 文件名称
     */
    public static String getAbsoluteFileDBF(String filename)
    {
        String downloadPath = Global.getExportPathDBF() + filename;
        File desc = new File(downloadPath);
        if (!desc.getParentFile().exists())
        {
            desc.getParentFile().mkdirs();
        }
        return downloadPath;
    }
}
