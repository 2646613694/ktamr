package com.ktamr.httpClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ktamr.service.HaConfigService;
import com.ktamr.service.InterfaceDataService;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;

/**
 * 发送参数到服务器端
 * @author ktamr
 */
@Component
public class Client {

    @Autowired
    private HaConfigService haConfigService;

    @Autowired
    private InterfaceDataService interfaceDataService;

    public String httpClient(String operatorName) throws Exception{
        //Map<String,Object> map = haConfigService.selectConfigByV("山西原平");
        if(1==1){
            List<Map<String,Object>> list = interfaceDataService.selectV();
            ObjectMapper mapper = new ObjectMapper();
//            String sendUrl = map.get("k").toString();
//            String unitCode = map.get("unitCode").toString();
//            String factoryCode = map.get("factoryCode").toString();
            String uploadData =  mapper.writeValueAsString(list);
            return getClient("http://122.224.74.182:7100/cwp/public/interface/dateUpload.do?method=upload","90703","004",uploadData);
        }
        return "无配置";
    }

    private String getClient(String sendUrl,String unitCode,String factoryCode,String uploadData) throws Exception{
        PostMethod post = null;
        String info = "";
        HttpClient httpClient = new HttpClient();
        try {
            post = new PostMethod(sendUrl);
            post.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
            post.addParameter("unitCode",unitCode);
            post.addParameter("factoryCode", factoryCode);
            post.addParameter("uploadData", uploadData);
            httpClient.executeMethod(post);
            info = new String(post.getResponseBody(), "UTF-8");

        }finally {
            post.releaseConnection();
        }
        return info;
    }

}
