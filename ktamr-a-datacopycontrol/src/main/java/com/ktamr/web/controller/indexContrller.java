package com.ktamr.web.controller;

import com.ktamr.common.config.Global;
import com.ktamr.ShiroUtils;
import com.ktamr.common.utils.sql.SqlCondition;
import com.ktamr.domain.HaCentor;
import com.ktamr.domain.HaGprsdtu;
import com.ktamr.domain.HaOperator;
import com.ktamr.domain.HaOperatorRgns;
import com.ktamr.service.HaCentorService;
import com.ktamr.service.HaGprsdtuService;
import com.ktamr.service.HaOperatorRgnsService;
import com.ktamr.service.HaOperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class indexContrller {

    @Resource
    private HaOperatorService haOperatorService;
    @Resource
    private HaOperatorRgnsService haOperatorRgnsService;
    @Resource
    private HaCentorService haCentorService;
    @Autowired
    private HaGprsdtuService haGprsdtuService;

    /**
     * 打开主页
     * @param mmap
     * @param session
     * @param haOperatorRgns
     * @return
     */
    @RequestMapping("/index")
    public String index(ModelMap mmap, HttpSession session, HaOperatorRgns haOperatorRgns){
        loginGetSession(session,new HaCentor(),new HaGprsdtu());
        session.setAttribute("webVersion", "V2.6.5");
        session.setAttribute("version", "2.6.5");
        session.setAttribute("haOperator", ShiroUtils.getHaOperator());
        session.setAttribute("operatorCode", ShiroUtils.getOperatorCode());
        HaOperator haOperator = (HaOperator)session.getAttribute("haOperator");
        session.setAttribute("haOperatorCompanyId",haOperator.getOperatorCompanyId());
        session.setAttribute("haOperatorRgnType",haOperator.getOperatorRgnType());//登录时候获取并存放haOperatorRgnType
        session.setAttribute("haOperatorLevel",haOperator.getOperatorLevel());

        //获取用户授权区域字符串 begin
        String operatorCode = (String)session.getAttribute("operatorCode");//第一个参数
        String operatorRgnType = haOperator.getOperatorRgnType();
        if(operatorRgnType!=null){
            operatorRgnType=operatorRgnType.trim();//第二个参数
        }
       if(!operatorRgnType.equals("rgn") && !operatorRgnType.equals("area")){
            session.setAttribute("rgnStr", "");
            session.setAttribute("leftRgnStr", "");
            return "index";
        }
        haOperatorRgns.setOperatorCode(operatorCode);
        List<HaOperatorRgns> sql1 = haOperatorRgnsService.sql1(haOperatorRgns);
        String rgnStr = "";
        String leftRgnStr = "";
        if(sql1!=null){

            for (int i=0;i<sql1.size();i++){
                rgnStr=rgnStr+sql1.get(i).getRgnCode()+",";
            }

            rgnStr= rgnStr.substring(0,rgnStr.lastIndexOf(","));
        }
        if (operatorRgnType.equals("area")){
            List<HaOperatorRgns> sql2 = haOperatorRgnsService.sql2(haOperatorRgns);
            for (int i=0;i<sql2.size();i++){
                leftRgnStr=leftRgnStr+sql2.get(i).getRgnCode()+",";
            }
            leftRgnStr= leftRgnStr.substring(0,leftRgnStr.lastIndexOf(","));
        }
        //获取用户授权区域字符串 end
        session.setAttribute("rgnStr", rgnStr);
        session.setAttribute("leftRgnStr", leftRgnStr);
        return "index";
    }

    /**
     * 左侧菜单根据以下session的值是否显示
     * @param session
     * @param haCentor
     * @param haGprsdtu
     */
    public void loginGetSession(HttpSession session, HaCentor haCentor, HaGprsdtu haGprsdtu){
        //查询手抄器其中的线路列表
        List<HaCentor> listHaCentor1 = haCentorService.selectAllCentorHandAndCount(haCentor);
        //查询手抄器其中的表列表
        //List<HaCentor> haCentors2 = haCentorService.selectAllCentorzQueryIdAndCount(haCentor);
        //查询DTU列表
        haGprsdtu.getParams().put("getRightCondition", SqlCondition.getRightCondition("a.areano","area","and"));
        List<HaGprsdtu> listHaCentor5 = haGprsdtuService.selectAllGprsdtuAndCount(haGprsdtu);

        //这里获取集中器，集采器，手抄器，DTU
       /* Integer centor_count = listHaCentor1.size();
        Integer collector_count = haCentors2.size();*/
        String publicUid = (String) session.getAttribute("publicUid");
        Integer centor_count = haCentorService.centor_count(publicUid);
        Integer collector_count = haCentorService.collector_count(publicUid);
        Integer pad_count1 = listHaCentor1.size();
        Integer pad_count2 = haCentorService.pad_count2(publicUid);
        Integer DTU_count = listHaCentor5.size();

        //这里获取集中器,集采器,手抄器,DTU
           /* Integer centor_count = haCentorService.centor_count(publicUid);
            Integer collector_count = haCentorService.collector_count(publicUid);
            Integer pad_count1 = haCentorService.pad_count1(publicUid);
            Integer pad_count2 = haCentorService.pad_count2(publicUid);
            Integer DTU_count = haCentorService.DTU_count(publicUid);*/
        session.setAttribute("centor_count",centor_count);
        session.setAttribute("collector_count",collector_count);
        session.setAttribute("pad_count1",pad_count1);
        session.setAttribute("pad_count2",pad_count2);
        session.setAttribute("DTU_count",DTU_count);
        //return  "success";
    }
    /**
     * 打开修改密码界面
     * @return
     */
    @RequestMapping("/operator/operator_pwd_change")
    public  String openPasswordChange(){
        return "operator/operatorPwdChange";
    }

    /**
     * 对其修改密码进行操作
     * @return
     */
    @RequestMapping("/operator_pwd_change_do")
    @ResponseBody
    public String operator_pwd_change_do(@RequestParam( value = "new_pwd") String new_pwd, HaOperator haOperator){
        String s = haOperatorService.CheckPwd(haOperator);//获取当前账户的密码
        if(s.equals(haOperator.getOperatorPwd())){
             if(new_pwd!=null){
                 haOperator.setOperatorPwd(new_pwd);
             }
            Integer changePWD = haOperatorService.ChangePWD(haOperator);//进行修改，返回受影响的行数
            if(changePWD!=null && changePWD>0){
                return "";//返回空代表操作成功
            }

        }
        return "原密码错误";
    }


}
